#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qsLang = new QSettings("/home/andrew/Documents/Git/qtProjects/MySQL_Editor/Settings/Lang.ini", QSettings::IniFormat);
    qsSettings = new QSettings("/home/andrew/Documents/Git/qtProjects/MySQL_Editor/Settings/Settings.ini", QSettings::IniFormat);

    currentTableName.clear();
    curLang = getIniValueInLatin1(qsSettings, "Settings/curLang", "eng");

    statusBarLabel_1 = new QLabel();
    db = QSqlDatabase::addDatabase("QMYSQL");
    bConnected = false;

    setLang();

    ui->tabWidget->setEnabled(false);
    //ui->tabWidget->setTabEnabled(0, false);
    ui->tabWidget->setTabEnabled(2, false);
    // add Label to status bar for displaying connection status;
    ui->statusBar->insertWidget(0,statusBarLabel_1);
    // for _debug
    //    qsLang->setValue("ru/sdgfdh", "VAlue");
}

MainWindow::~MainWindow()
{
    delete qsLang;
    delete qsSettings;
    delete statusBarLabel_1;
    delete ui;
}

void MainWindow::on_pushButton_clicked() // Connect button
{
    LogIn();
}

void MainWindow::on_pushButton_2_clicked() // Add Table button
{
    stateCode = 3;
    if(!bConnected) { ConnectionAlert(); return; }
    //--------------------------------------------------------
    ui->tabWidget->setEnabled(true);
    ui->tabWidget->setCurrentIndex(2);
    ui->tabWidget->setTabEnabled(0, false);
    ui->tabWidget->setTabEnabled(1, false);
    ui->tabWidget->setTabEnabled(2, true);
    //---------------------------------------------------------
    ui->tableWidget_2->clear();
    ui->tableWidget_2->setColumnCount(0);
    ui->tableWidget_2->setRowCount(0);
    ui->tableWidget_3->clear();
    ui->tableWidget_3->setColumnCount(0);
    ui->tableWidget_3->setRowCount(0);
}

void MainWindow::on_pushButton_3_clicked() // Edit record
{
    stateCode = 2;
    if(!bConnected) { ConnectionAlert(); return; }
    //--------------------------------------------------------
    int curColumn   = ui->tableWidget->currentColumn(),
        curRow      = ui->tableWidget->currentRow();
//    QMessageBox::warning(this, "DEBUG", QVariant(curColumn).toString()+QVariant(curRow).toString());

    if(curColumn==-1 || curRow == -1) {
        QMessageBox::warning(this, "Cell not selected", "Cell isn't selected!");
        return;
    }
    //---------------------------------------------------------------------------------
    QString condition = "";
    for(int i = 0; i < ui->tableWidget->columnCount(); i++) {
        condition += ui->tableWidget->horizontalHeaderItem(i)->text() + "='"
                + ui->tableWidget->item(curRow, i)->text();
        if(i != ui->tableWidget->columnCount() - 1)
            condition += "' && ";
        else
            condition += "'";
    }
    requestString = "SELECT * FROM " + currentTableName + " WHERE " + condition;
    //----------------------------------------------------------------------------------
    if(!this->fillTable(requestString, ui->tableWidget_2)) { ShowDbError(); return; }
    this->describeTable(currentTableName, ui->tableWidget_3);
    //-------------------------------------------------------
    ui->tabWidget->setTabEnabled(0, false);
    ui->tabWidget->setTabEnabled(1, false);
    ui->tabWidget->setTabEnabled(2, true);
}

void MainWindow::on_pushButton_4_clicked() // Add record
{
    stateCode = 1;
    if(!bConnected) { ConnectionAlert(); return; }
    //--------------------------------------------------------
    this->describeTable(currentTableName, ui->tableWidget_3);
    //--------------------------------------------------------
    QStringList columnsNames;    // contains columns names in Horizontal Header
    for(int i = 0; i < ui->tableWidget->columnCount(); i++) {
        columnsNames.append(ui->tableWidget->horizontalHeaderItem(i)->text());
    }
    //-------------------------------------------------------
    ui->tableWidget_2->setColumnCount(ui->tableWidget->columnCount());
    ui->tableWidget_2->setRowCount(1);
    ui->tableWidget_2->setHorizontalHeaderLabels(columnsNames); //set columns names in Horizontal Header
    ui->tableWidget_2->verticalHeader()->setVisible(false); //hide Vertical Header
    //-------------------------------------------------------
    ui->tabWidget->setTabEnabled(0, false);
    ui->tabWidget->setTabEnabled(1, false);
    ui->tabWidget->setTabEnabled(2, true);
}

void MainWindow::on_pushButton_5_clicked() // Delete record button
{
    if(!bConnected) { ConnectionAlert(); return; }
    //-------------------------------------------------------------
    int curRow = ui->tableWidget->currentRow();
    if(curRow == -1) {
        QMessageBox::warning(this, "Cell not selected", "Cell isn't selected!");
        return;
    }
    //-------------------------------------------------------------
    QString condition = "";
    for(int i = 0; i < ui->tableWidget->columnCount(); i++) {
        condition += ui->tableWidget->horizontalHeaderItem(i)->text() + "='"
                + ui->tableWidget->item(curRow, i)->text();
        if(i != ui->tableWidget->columnCount() - 1)
            condition += "' && ";
        else
            condition += "'";
    }
    //--------------------------------------------------------------
    requestString = "DELETE FROM " + currentTableName + " WHERE " + condition;
//    QMessageBox::information(this, "DEBUG", requestString);
    QSqlQuery query = db.exec(requestString);
    if(query.lastError().nativeErrorCode().toInt()) {
        ShowDbError();
    } else {
        requestString = "SELECT * FROM " + currentTableName;
        if(!this->fillTable(requestString, ui->tableWidget)) { ShowDbError();}
    }
}

void MainWindow::on_pushButton_6_clicked() // Execute button
{
    ui->textEdit->insertPlainText("sfkjdslgs\n");
    ui->textEdit->insertHtml("<span style='color:red;'>dgklhsdfjhgsdkfgjws</span>");
}

void MainWindow::on_pushButton_7_clicked() // Save changes button
{
    if(!bConnected) { ConnectionAlert(); return; }
    //------------------------------------------------------------
    if(stateCode == 1) {
        QString columns = "", values = "";
        for(int i = 0; i < ui->tableWidget_2->columnCount(); i++) {
            //columnsNames.append(ui->tableWidget->horizontalHeaderItem(i)->text());
            if(i != ui->tableWidget_2->columnCount() - 1) {
                columns += ui->tableWidget_2->horizontalHeaderItem(i)->text() + ", ";
                values  += "'" + ui->tableWidget_2->item(0, i)->text() + "', ";
            } else {
                columns += ui->tableWidget->horizontalHeaderItem(i)->text();
                values  += "'" + ui->tableWidget_2->item(0, i)->text() + "'";
            }
        }
        //------------------------------------------------------------
        requestString = "INSERT INTO " + currentTableName + " (" + columns + ") VALUES (" + values + ")";
        //------------------------------------------------------------------------------------------------
    } else if (stateCode == 2) {
        int curRow = ui->tableWidget->currentRow();
        QString condition = "", values = "";
        for(int i = 0; i < ui->tableWidget->columnCount(); i++) {
            condition += ui->tableWidget->horizontalHeaderItem(i)->text() + "='"
                    + ui->tableWidget->item(curRow, i)->text();
            values += ui->tableWidget_2->horizontalHeaderItem(i)->text() + "='"
                    + ui->tableWidget_2->item(0, i)->text() + "'";
            if(i != ui->tableWidget->columnCount() - 1) {
                condition += "' && ";
                values += ", ";
            } else {
                condition += "'";
                values += " ";
            }
        }
        //-------------------------------------------------------------
        requestString = "UPDATE " + currentTableName + " SET " + values + " WHERE " + condition;
//        QMessageBox::warning(this, "DEBUG", requestString);
        //------------------------------------------------------------------------------------------------
    } else {
        /*CRETE TABLE dfghsddfsh (INT NOT NULL AUTO_INCREMENT , VARCHAR NOT NULL , FLOAT NULL , sdgsdg)KEY(sdabfhd)sdfs)*/

        QComboBox *temp;
        QString fieldsDescr = "";
        QString fieldsKeys = "";

        for(int i = 0; i < ui->tableWidget_3->rowCount(); i++)
        {
            fieldsDescr += ui->tableWidget_3->item(i, 0)->text() + " ";
            for(int j = 1; j <= 5; j++) {
                if(j==4) continue;
                temp = qobject_cast<QComboBox*>(ui->tableWidget_3->cellWidget(i,j));
                if(j==3) {
                    if(temp->currentText()!= "NONE") {
                        if(fieldsKeys != "")
                            fieldsKeys += ", ";
                        if(temp->currentText() == "PRIMARY")
                            fieldsKeys += "PRIMARY KEY(";
                        if(temp->currentText() == "MUL")
                            fieldsKeys += "KEY(";
                        fieldsKeys += ui->tableWidget_3->item(i, 0)->text() + ")";
                    }
                } else {
                    //temp = qobject_cast<QComboBox*>(ui->tableWidget->cellWidget(i,j));
                    if(temp->currentText() != "NONE")
                        fieldsDescr += temp->currentText() + " ";
                }
            }
            //----------------------------------------
            if(fieldsKeys != "" || i < ui->tableWidget_3->rowCount() - 1)
                fieldsDescr += ", ";
        }
        requestString = "CREATE TABLE " + ui->lineEdit->text() + " (" + fieldsDescr + fieldsKeys + ")";
        QMessageBox::warning(this, "DEBUG", requestString);
    }
    QSqlQuery query = db.exec(requestString);
    if(query.lastError().nativeErrorCode().toInt()) {
        ShowDbError();
    } else {
        if(stateCode == 3) currentTableName = ui->lineEdit->text();
        //------------------------------------------------
        requestString = "SELECT * FROM " + currentTableName;
        if(!this->fillTable(requestString, ui->tableWidget)) { ShowDbError();}
        //------------------------------------------------
        ui->tableWidget->setCurrentCell(0, 0);
    }
    //------------------------------------------------------------
    StopEditing();
}

void MainWindow::on_pushButton_8_clicked() // Add Column button
{
    QStringList horizontalHeaderLabels, types, keys, extraParams;
    //-----------------------------------------------------------------
    horizontalHeaderLabels.append("Field");
    horizontalHeaderLabels.append("Type");
    horizontalHeaderLabels.append("Null");
    horizontalHeaderLabels.append("Key");
    horizontalHeaderLabels.append("Default");
    horizontalHeaderLabels.append("Extra");
    //---------------
    types.append("INT");
    types.append("SMALLINT");
    types.append("CHAR");
    types.append("VARCHAR");
    types.append("STRING");
    types.append("DATE");
    types.append("TIME");
    types.append("FLOAT");
    //---------------
    keys.append("NONE");
    keys.append("PRIMARY");
    keys.append("MUL");
    //---------------
    extraParams.append("NONE");
    extraParams.append("AUTO_INCREMENT");
    //------------------------------------------------------------------
    QComboBox *type = new QComboBox;
    QComboBox *null = new QComboBox;
    QComboBox *key = new QComboBox;
    QComboBox *extra = new QComboBox;
    //------------------------------------------------------------------
    type->insertItems(0, types);
    null->insertItem(0, "NOT NULL", 0);
    null->insertItem(1, "NULL", 0);
    key->insertItems(0, keys);
    extra->insertItems(0, extraParams);
    //------------------------------------------------------------------
    ui->tableWidget_3->setColumnCount(horizontalHeaderLabels.count());
    ui->tableWidget_3->setHorizontalHeaderLabels(horizontalHeaderLabels);
    //------------------------------------------------------------------
    int placeRow;
    if(ui->tableWidget_3->currentRow() != -1) {
        placeRow = ui->tableWidget_3->currentRow()+1;
        ui->tableWidget_3->insertRow(placeRow);
    } else {
        placeRow = ui->tableWidget_3->rowCount();
        ui->tableWidget_3->setRowCount(placeRow+1);
    }
    //------------------------------------------------------------------
    ui->tableWidget_3->setCellWidget(placeRow, 1, type);
    ui->tableWidget_3->setCellWidget(placeRow, 2, null);
    ui->tableWidget_3->setCellWidget(placeRow, 3, key);
    ui->tableWidget_3->setCellWidget(placeRow, 5, extra);
    //------------------------------------------------------------------
    ui->tableWidget_3->resizeColumnsToContents();
}

void MainWindow::on_pushButton_9_clicked() //Remove column button
{
    if(ui->tableWidget_3->currentRow() != -1)
        ui->tableWidget_3->removeRow(ui->tableWidget_3->currentRow());
    else
        ui->tableWidget_3->removeRow(ui->tableWidget_3->rowCount() - 1);
}

void MainWindow::on_listWidget_itemDoubleClicked(QListWidgetItem *item) //changed table on tables list
{
    currentTableName = item->text();
    requestString = "SELECT * FROM " + currentTableName;
    if(!this->fillTable(requestString, ui->tableWidget)) { ShowDbError(); return; }
    //------------------------------------------------
    ui->tableWidget->setCurrentCell(0, 0);
    //------------------------------------------------
    ui->tabWidget->setEnabled(true);
    StopEditing();
}

void MainWindow::setLang() // Set up localization to program
{
    ui->label_1->setText(getIniValueInLatin1(qsLang, curLang+"/MWlabel_1", "DB: "));
    ui->label_2->setText(getIniValueInLatin1(qsLang, curLang+"/MWlabel_2", "Table name:"));
    ui->label->setText(getIniValueInLatin1(qsLang, curLang+"/MWlabel", "Table description"));

    ui->groupBox->setTitle(getIniValueInLatin1(qsLang, curLang+"/MWgroupBox", "Tables list"));
    // Push Buttons
    ui->pushButton->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_1", "Connect"));
    ui->pushButton_2->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_2", "Add Table"));
    ui->pushButton_3->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_3", "Edit record"));
    ui->pushButton_4->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_4", "Add record"));
    ui->pushButton_5->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_5", "Delete record"));
    ui->pushButton_6->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_6", "Execute"));
    ui->pushButton_7->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_7", "Save changes"));
    ui->pushButton_8->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_8", "Add column"));
    ui->pushButton_9->setText(getIniValueInLatin1(qsLang, curLang+"/MWpushButton_9", "Remove column"));
    // Tab Widget
    ui->tabWidget->setTabText(0, getIniValueInLatin1(qsLang, curLang+"/MWtab_1", "Table"));
    ui->tabWidget->setTabText(1, getIniValueInLatin1(qsLang, curLang+"/MWtab_2", "Console"));
    ui->tabWidget->setTabText(2, getIniValueInLatin1(qsLang, curLang+"/MWtab_3", "Editing"));

}

void MainWindow::LogIn()
{ // This function setup connection to MySQL database
    QStringList connectionData;                 // contains list of MySQL connection parameters
    ConnectionDataDialog connectionDataDialog;  // Displays connecton parametrs dialog

    if(!connectionDataDialog.exec()) return;    // if clicked on [Cancel]

    connectionData = connectionDataDialog.getConnectionData();  // Gets connection parametrs list
    db.setHostName(connectionData[0]);
    db.setDatabaseName(connectionData[1]);
    db.setUserName(connectionData[2]);
    db.setPassword(connectionData[3]);
    db.setPort(connectionData[4].toInt());

    if (!db.open()) {
       ShowDbError();
       statusBarLabel_1->setText(getIniValueInLatin1(qsLang, curLang+"/MWConnStatusFailed", "Connection: failed;"));
    } else {
        bConnected = true;
        statusBarLabel_1->setText(getIniValueInLatin1(qsLang, curLang+"/MWConnStatusSuccesful", "Connection: succesful;"));
        ui->label_1->setText(getIniValueInLatin1(qsLang, curLang+"/MWlabel_1", "DB: ") + " " + db.databaseName());

        this->fillTablesList();
    }
}

void MainWindow::fillTablesList()
{
    QSqlQuery query = db.exec("SHOW TABLES");
    if(query.lastError().nativeErrorCode().toInt()) { ShowDbError(); return; }
    // Clear tables list if it's contain items
    if(ui->listWidget->count() != 0)
        ui->listWidget->clear();
//    QMessageBox::warning(this, "DEBUG", query.lastError().nativeErrorCode().toInt());
    while(query.next()) {
        ui->listWidget->addItem(query.value(0).toString());
    }
}

bool MainWindow::fillTable(QString requestString, QTableWidget* tableWidget)
{
    QStringList columnsNames;                   // contains columns names in Horizontal Header
    QSqlQuery query = db.exec(requestString);   // contains request

    if(query.lastError().nativeErrorCode().toInt()) { ShowDbError(); return false; }

    int columnsCount = query.record().count();
    int rowsCount = query.size();

    tableWidget->setColumnCount(columnsCount);
    tableWidget->setRowCount(rowsCount);

    for(int i = 0; i < columnsCount; i++)
        columnsNames.append(query.record().fieldName(i));

    int index=0;
    while (query.next()) {
        for(int i = 0; i < columnsCount; i++)
            tableWidget->setItem(index,i,new QTableWidgetItem(query.value(i).toString()));
        index++;
    }
    tableWidget->setHorizontalHeaderLabels(columnsNames); //set columns names in Horizontal Header
    tableWidget->verticalHeader()->setVisible(false); //hide Vertical Header
    // resizes ---------------------------------
    tableWidget->resizeRowsToContents();
    tableWidget->resizeColumnsToContents();

    return true;
}

void MainWindow::describeTable(QString tableName, QTableWidget* tableWidget)
{
    requestString = "DESCRIBE " + tableName;
    //-------------------------------------------------------------------------
    QStringList columnsNames;    // contains columns names in Horizontal Header
    QSqlQuery query = db.exec(requestString);   // contains request
    //-------------------------------------------------------------------------
    if(query.lastError().nativeErrorCode().toInt()) { ShowDbError(); return; }

    int columnsCount = query.record().count();
    int rowsCount = query.size();

    tableWidget->setColumnCount(columnsCount);
    tableWidget->setRowCount(rowsCount);

    int index=0;
    while (query.next()) {
        for(int i = 0; i < columnsCount; i++) {
            columnsNames.append(query.record().fieldName(i));
            if((i==4)&&(query.value(i).toString() == ""))
                tableWidget->setItem(index,i,new QTableWidgetItem("NULL"));
            else
                tableWidget->setItem(index,i,new QTableWidgetItem(query.value(i).toString()));
        }
        index++;
    }
    tableWidget->setHorizontalHeaderLabels(columnsNames); //set columns names in Horizontal Header
    tableWidget->verticalHeader()->setVisible(false); //hide Vertical Header
    // resizes ---------------------------------
    tableWidget->resizeRowsToContents();
    tableWidget->resizeColumnsToContents();
}

void MainWindow::ShowDbError()
{
    QMessageBox::critical(this,
             getIniValueInLatin1(qsLang, curLang+"/MDBError","Database Error"),
               "Code: "          + db.lastError().nativeErrorCode()
             + "\nDescription: " + db.lastError().text()
             + "\nRequest: "     + requestString);
}

void MainWindow::ConnectionAlert()
{
    QMessageBox::warning(this,
        getIniValueInLatin1(qsLang, curLang+"/MNotConnectedHeader", "Not connected"),
        getIniValueInLatin1(qsLang, curLang+"/MNotConnectedMessage", "You must connect to database first!" ));
}

void MainWindow::StopEditing() //empty tabWidget_2 & tabWidget_3
{
    ui->tabWidget->setCurrentIndex(0);
    ui->tabWidget->setTabEnabled(0, true);
    ui->tabWidget->setTabEnabled(1, true);
    ui->tabWidget->setTabEnabled(2, false);
    //---------------------------------------------------------
    ui->tableWidget_2->clear();
    ui->tableWidget_2->setColumnCount(0);
    ui->tableWidget_2->setRowCount(0);
    ui->tableWidget_3->clear();
    ui->tableWidget_3->setColumnCount(0);
    ui->tableWidget_3->setRowCount(0);
}
